
/// Byday is a data type that represents BYDAY node.
class ByMonthday {
  ByMonthday(this._monthday);

  final List<int> _monthday;

  /// getMonthday returns a list of monthdays(s) that is specified by BYMONTHDAY.
  List<int> getMonthday() => _monthday;
}
