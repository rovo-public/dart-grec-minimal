
/// BySetPos is a data type that represents BYSETPOS node.
class BySetPos {
  BySetPos(this._pos);

  final int _pos;

  /// getSetPos returns pos that is specified by BYSETPOS.
  int getSetPos() => _pos;
}
