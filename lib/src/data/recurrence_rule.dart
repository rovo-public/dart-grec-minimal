import 'package:grec_minimal/src/data/byday.dart';
import 'package:grec_minimal/src/data/bymonthday.dart';
import 'package:grec_minimal/src/data/bysetpos.dart';
import 'package:grec_minimal/src/exception/conditional_exception.dart';
import 'package:grec_minimal/src/symbol/frequency.dart';
import 'package:grec_minimal/src/symbol/weekday.dart';

/// RecurrenceRule is a data type that represents recurrence rule of calendar.
class RecurrenceRule {
  RecurrenceRule(
    Frequency? frequency,
    int? count,
    DateTime? until,
    int? interval,
    Byday? byday,
    ByMonthday? bymonthday,
    BySetPos? bySetPos,
    DateTime? dtstart,
  ) {
    if (frequency != null) {
      _frequency = frequency;
    }

    if (interval != null) {
      _interval = interval;
    }

    if (dtstart != null) {
      _dtstart = dtstart;
    }

    if (bySetPos != null && bySetPos.getSetPos() == 0) {
      throw new ConditionalException(
          'conflicted. it is prohibited to specify empty `BYSETPOS`.');
    } else if (bySetPos != null) {
      _bySetPos = bySetPos;
    }

    if (bymonthday != null && bymonthday.getMonthday().isEmpty) {
      throw new ConditionalException(
          'conflicted. it is prohibited to specify empty `BYMONTHDAY`.');
    } else if (bymonthday != null) {
      _bymonthday = bymonthday;
    }

    if (byday != null &&
        byday.getNth() != null &&
        byday.getWeekday().length >= 2) {
      throw new ConditionalException(
          'conflicted. it is prohibited to specify `Nth` of `BYDAY` with multiple weekdays.');
    } else if (byday != null) {
      _byday = byday;
    }

    if (count != null && until != null) {
      throw new ConditionalException(
          'conflicted. it is prohibited to specify `COUNT` and `UNTIL` together.');
    } else if (count != null) {
      _count = count;
    } else if (until != null) {
      _until = until;
    }

    if (frequency == Frequency.DAILY && byday != null) {
      throw new ConditionalException(
          'conflicted. it is prohibited to specify `BYDAY` when `FREQ` is DAILY.');
    }

    if (frequency == Frequency.WEEKLY &&
        byday != null &&
        byday.getNth() != null) {
      throw new ConditionalException(
          'conflicted. it is prohibited to specify `Nth` of `BYDAY` when `FREQ` is WEEKLY.');
    }

    if (frequency == Frequency.YEARLY && byday != null) {
      throw new ConditionalException(
          'conflicted. it is prohibited to specify `BYDAY` when `FREQ` is YEARLY.');
    }
  }

  late Frequency _frequency;
  int? _count;
  DateTime? _dtstart;
  DateTime? _until;
  int? _interval;
  Byday? _byday;
  ByMonthday? _bymonthday;
  BySetPos? _bySetPos;

  /// getFrequency returns frequency of recurrence that is specified by FREQ.
  Frequency getFrequency() => _frequency;

  /// getCount returns count of recurrence that is specified by COUNT.
  int? getCount() => _count;

  /// getUntil returns until of recurrence that is specified by UNTIL.
  DateTime? getUntil() => _until;

  /// getDtstart returns dtstart of recurrence that is specified by DTSTART.
  DateTime? getDtstart() => _dtstart;

  /// getInterval returns interval of recurrence that is specified by INTERVAL.
  int? getInterval() => _interval;

  /// getByday returns byday of recurrence that is specified by BYDAY.
  Byday? getByday() => _byday;

  /// getByMonthday returns bymonthday of recurrence that is specified by BYMONTHDAY.
  ByMonthday? getByMonthday() => _bymonthday;

  /// getBySetPos returns bysetpos of recurrence that is specified by BYSETPOS.
  BySetPos? getBySetPos() => _bySetPos;

  /// asRuleText dumps the rule as a text.
  String asRuleText() {
    String start = '';
    if (_dtstart != null) {
      String dtstart = _dtstart!
          .toIso8601String()
          .replaceAll(new RegExp(r'(?:[-:]|[.]\d+)'), '');
      if (!dtstart.endsWith('Z')) {
        dtstart += 'Z';
      }
      start = 'DTSTART:${dtstart}\n';
    }
    final StringBuffer sb = new StringBuffer(
        '${start}RRULE:FREQ=${FrequencyOperator.getSimpleName(_frequency)}');

    if (_count != null) {
      sb.write(';COUNT=$_count');
    }

    if (_until != null) {
      final String untilStr = _until!
          .toIso8601String()
          .replaceAll(new RegExp(r'(?:[-:]|[.]\d+)'), '');
      sb.write(';UNTIL=$untilStr');
      if (!untilStr.endsWith('Z')) {
        sb.write('Z');
      }
    }

    if (_interval != null) {
      sb.write(';INTERVAL=$_interval');
    }

    if (_byday != null && _byday!.getNth() != null) {
      final int? nth = _byday!.getNth();
      final String weekdays =
          _byday!.getWeekday().map(WeekdayOperator.getSimpleName).join(',');
      sb.write(';BYDAY=${nth ?? ''}$weekdays');
    }

    if (_bymonthday != null) {
      final String monthDays = _bymonthday!.getMonthday().join(',');
      sb.write(';BYMONTHDAY=$monthDays');
    }

    if (_bySetPos != null) {
      final int pos = _bySetPos!.getSetPos();
      sb.write(';BYSETPOS=$pos');
    }

    return sb.toString();
  }
}
