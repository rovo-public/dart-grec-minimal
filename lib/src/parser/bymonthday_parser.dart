import 'package:grec_minimal/src/data/bymonthday.dart';
import 'package:grec_minimal/src/exception/invalid_syntax_exception.dart';
import 'package:grec_minimal/src/parser/parsable.dart';
import 'package:grec_minimal/src/parser/parse_result.dart';

import '../../grec_minimal.dart';

class ByMonthdayParser implements Parsable<ByMonthday> {
  static final RegExp _bymonthdayRE = new RegExp(r'BYMONTHDAY=([^;]+);?');

  @override
  ParseResult<ByMonthday> parse(final String subject) {
    final Iterable<Match> matches = _bymonthdayRE.allMatches(subject);

    if (matches.isEmpty) {
      return new ParseResult<ByMonthday>(subject, null);
    }

    if (matches.length >= 2) {
      throw new InvalidSyntaxException(
          'syntax error: `BYMONTHDAY` can appear to once at most.');
    }

    final Match match = matches.first;
    final List<String> monthdays = match.group(1)!.split(',');

    return new ParseResult<ByMonthday>(
      subject.replaceAll(_bymonthdayRE, ''),
      new ByMonthday(
        monthdays.map((String day) => int.parse(day, radix: 10)).toList(),
      ),
    );
  }
}
