import 'package:grec_minimal/src/data/bysetpos.dart';
import 'package:grec_minimal/src/exception/invalid_syntax_exception.dart';
import 'package:grec_minimal/src/parser/parsable.dart';
import 'package:grec_minimal/src/parser/parse_result.dart';

class BySetPosParser implements Parsable<BySetPos> {
  static final RegExp _bysetPosRE = new RegExp(r'BYSETPOS=([^;]+);?');

  @override
  ParseResult<BySetPos> parse(final String subject) {
    final Iterable<Match> matches = _bysetPosRE.allMatches(subject);

    if (matches.isEmpty) {
      return new ParseResult<BySetPos>(subject, null);
    }

    if (matches.length >= 2) {
      throw new InvalidSyntaxException(
          'syntax error: `BYSETPOS` can appear to once at most.');
    }

    final Match match = matches.first;
    final int pos = int.parse(match.group(1) ?? '');

    return new ParseResult<BySetPos>(
      subject.replaceAll(_bysetPosRE, ''),
      new BySetPos(
        pos,
      ),
    );
  }
}
