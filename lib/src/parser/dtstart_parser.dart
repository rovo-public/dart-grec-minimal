import 'package:grec_minimal/src/exception/invalid_syntax_exception.dart';
import 'package:grec_minimal/src/parser/parsable.dart';
import 'package:grec_minimal/src/parser/parse_result.dart';

class DtstartParser implements Parsable<DateTime> {
  static final RegExp _dtstartRE = new RegExp(r'DTSTART:([0-9]{8}T[0-9]{6}Z);?');

  @override
  ParseResult<DateTime> parse(final String subject) {
    final Iterable<Match> matches = _dtstartRE.allMatches(subject);

    if (matches.isEmpty) {
      return new ParseResult<DateTime>(subject, null);
    }

    if (matches.length >= 2) {
      throw new InvalidSyntaxException(
          'syntax error: `DTSTART` can appear to once at most.');
    }

    final Match dtstartMatched = matches.single;
    return new ParseResult<DateTime>(
      subject.replaceAll(_dtstartRE, ''),
      DateTime.parse(dtstartMatched.group(1) ?? ''),
    );
  }
}
